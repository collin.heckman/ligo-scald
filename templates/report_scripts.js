<script>
  var app = new Vue({
    el: '#report',
    data: function () {
      return {
        totalRows: {},
        perPage: 10,
        currentPage: {},
        fields: {},
        items: {},
        plotdata: {},
        plotopts: {},
        plotlayout: {}
      }
    },
    methods: {
      getTableJson: function (url) {
        axios
          .get(url)
          .then(response => {
            Vue.set(this.items, url, response.data.items);
            Vue.set(this.fields, url, response.data.fields);
            Vue.set(this.totalRows, url, response.data.items.length);
            Vue.set(this.currentPage, url, 1);
          })
      },
      getPlotJson: function (url, div) {
        axios
          .get(url)
          .then(response => {
            Vue.set(this.plotdata, url, response.data);
            Vue.set(this.plotopts, url, {});
            Vue.set(this.plotlayout, url, {});
            return response.data;
          })
          .then(data => {
            Plotly.react(div, this.plotdata[url], {}, {});
          })
      }
    },
    delimiters: ['[[',']]'],
  });

% for i, c in enumerate(content):
%    if c['name'] == 'table':
       app.getTableJson('{{ c['url'] }}');
%    elif c['name'] == 'plot':
%      div_suffix = c['title'].lower().replace(' ', '_').replace(':','').replace('.', '')
       app.getPlotJson('{{ c['url'] }}', 'plot_{{ div_suffix }}');
%    end
% end
</script>
