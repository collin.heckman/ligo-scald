#!/usr/bin/env python

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__description__ = "a module to store commonly used utility functions"

#-------------------------------------------------
### imports

import argparse
from collections import namedtuple
import functools
import json
import os
import random
import re
import sys
import time
import timeit

import h5py
import numpy
from scipy.interpolate import interp1d

import lal
from lal import gpstime

from . import aggregator


#-------------------------------------------------
### data utilities

def stats_on_data(data):
    return float(numpy.min(data)), float(numpy.percentile(data, 15.9)), float(numpy.percentile(data, 84.1)), float(numpy.max(data))


#-------------------------------------------------
### aggregation utilities

def gps_range_to_process(jobtime, dt=1):
    if jobtime:
        gpsblocks = set((floor_div(t, dt) for t in jobtime))
        if not gpsblocks:
            return [], []
        min_t, max_t = min(gpsblocks), max(gpsblocks)
        return zip(range(min_t, max_t + dt, dt), range(min_t + dt, max_t + 2*dt, dt))
    else:
        return None


def span_to_process(start, end, dt=1):
    return floor_div(start, dt), floor_div(end, dt) + dt


def duration_to_dt(duration):
    if duration <= 1000:
        dt = 1
    elif duration <= 10000:
        dt = 10
    elif duration <= 100000:
        dt = 100
    elif duration <= 1000000:
        dt = 1000
    elif duration <= 10000000:
        dt = 10000
    else:
        dt = 100000

    return dt


#-------------------------------------------------
### time utilities

def in_new_epoch(new_gps_time, prev_gps_time, gps_epoch):
    """
    Returns whether new and old gps times are in different
    epochs.

    >>> in_new_epoch(1234561200, 1234560000, 1000)
    True
    >>> in_new_epoch(1234561200, 1234560000, 10000)
    False

    """
    return (new_gps_time - floor_div(prev_gps_time, gps_epoch)) >= gps_epoch


def floor_div(x, n):
    """
    Floor a number by removing its remainder
    from division by another number n.

    >>> floor_div(163, 10)
    160
    >>> floor_div(158, 10)
    150

    """
    assert n > 0

    if isinstance(x, int) or (isinstance(x, numpy.ndarray) and numpy.issubdtype(x.dtype, numpy.integer)):
        return (x // n) * n
    elif isinstance(x, numpy.ndarray):
        return (x.astype(float) // n) * n
    else:
        return (float(x) // n) * n


def gps_now():
    """
    Returns the current gps time.
    """
    return float(lal.GPSTimeNow()) + (timeit.default_timer() % 1)


def gps_to_latency(gps_time):
    """
    Given a gps time, measures the latency to ms precision relative to now.
    """
    return numpy.round(gps_now() - gps_time, 3)


def rfc3339_to_gps(time_str):
    """
    converts an rfc3339-formatted string (UTC+0 only) to a valid gps time.
    """
    if time_str[-1] != 'Z':
        raise ValueError('missing Z indicating UTC+0')

    return float(gpstime.str_to_gps(time_str[:-1]))


def gps_to_unix(gps_time):
    """
    Converts from GPS to UNIX time, allows use of numpy arrays or scalars.
    """
    if isinstance(gps_time, numpy.ndarray):
        leapseconds = lal.GPSLeapSeconds(int(gps_time[0]))
        return ((gps_time + lal.EPOCH_UNIX_GPS - leapseconds) * 1e9).astype(int)
    else:
        leapseconds = lal.GPSLeapSeconds(int(gps_time))
        return int((gps_time + lal.EPOCH_UNIX_GPS - leapseconds) * 1e9)


def unix_to_gps(unix_time):
    """
    Converts from UNIX to GPS time, allows use of numpy arrays or scalars.
    """
    ### FIXME: doesn't handle leapseconds correctly, should use lal.GPSLeapSeconds
    return (unix_time / 1e9) - lal.EPOCH_UNIX_GPS + 18


#-------------------------------------------------
### nagios utilities

def status_to_nagios_response(text_status, bad_status):
    return {
        "nagios_shib_scraper_ver": 0.1,
        "status_intervals": [{
            "num_status": 2 if bad_status else 0,
            "txt_status": text_status,
        }],
    }


def extract_alert_tags(alert_settings):
    if 'tags' in alert_settings:
        return alert_settings['tags']
    else:
        tag_type = alert_settings['tag_type']
        alert_tag_format = alert_settings['tag_format']
        if 'digit' in alert_tag_format:
            num_digits = int(alert_tag_format[0])
            num_tags = int(alert_settings['num_tags'])
            tag_start = int(alert_settings['tag_start']) if 'tag_start' in alert_settings else 0
            return [(tag_type, str(tag_num).zfill(num_digits)) for tag_num in range(tag_start, tag_start+num_tags)]
        else:
            raise ValueError('{} tag format not recognized'.format(alert_tag_format))


#-------------------------------------------------
### parsing utilities

def append_subparser(subparser, cmd, func):
    assert func.__doc__, "empty docstring: {}".format(func)
    help_ = func.__doc__.split('\n')[0].lower().strip('.')
    desc = func.__doc__.strip()

    parser = subparser.add_parser(
        cmd,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help=help_,
        description=desc
    )

    parser.set_defaults(func=func)
    return parser


#-------------------------------------------------
### multiprocessing utilities

def unpack(func):
    """
    Unpacks an argument tuple and calls the target function 'func'.
    Used as a workaround for python 2 missing multiprocessing.Pool.starmap.

    Implemented from https://stackoverflow.com/a/52671399.

    """
    @functools.wraps(func)
    def wrapper(arg_tuple):
        return func(*arg_tuple)
    return wrapper
