#!/usr/bin/env python

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__description__ = "tools to serve data and dynamic html pages"

#-------------------------------------------------
### imports

import copy
import functools
import os
import pkg_resources
import sys
import time

try: ### import a faster json lib if available
    import ujson as json
except ImportError:
    import json

from six.moves import urllib

import bottle
import yaml

from . import aggregator
from . import io
from . import utils

#-------------------------------------------------
### bottle configuration/templates

JSON_HEADER = {
    'Content-type': 'application/json',
    'Cache-Control': 'max-age=10',
}

### load templates
template_path = pkg_resources.resource_filename(pkg_resources.Requirement.parse('ligo_scald'), 'templates')
bottle.TEMPLATE_PATH.insert(0, template_path)

### instantiate app
app = bottle.Bottle()


#-------------------------------------------------
### bottle apps/routing

@app.route("/static/<file_>")
def static(file_):
    """Route to serve static files, e.g. css, js.

    Parameters
    ----------
    file_ : `str`
        the file to serve

    """
    static_dir = pkg_resources.resource_filename(pkg_resources.Requirement.parse('ligo_scald'), 'static')
    yield bottle.static_file(file_, root=static_dir)


@app.route("/")
def index():
    """Route to serve a dashboard.

    """
    config = dict(app.config)
    static_dir = '../' if config['use_cgi'] else ''

    if "gps" in bottle.request.query:
        config['dashboard']['gps']=bottle.request.query['gps']
    if "duration" in bottle.request.query:
        config['dashboard']['duration']=bottle.request.query['duration']

    ### find out whether query is for realtime or historical data
    ### NOTE: gps < 0 is used as a proxy for realtime data and refers
    ###       to the delay (i.e. -5 means to query up to now - 5s
    gpstime = int(config['dashboard']['gps'])
    if gpstime < 0:
        delay = -gpstime
        stop = aggregator.now() - delay
        refresh = 2000
    else:
        delay = 0
        stop = gpstime
        refresh = -1
    start = stop - int(config['dashboard']['duration'])

    ### generate html
    yield bottle.template(
        'dashboard.html',
        static_dir=static_dir,
        script_name=config['script_name'],
        plots=config['plots'],
        start=start,
        stop=stop,
        refresh=refresh,
        delay=delay,
        dashboard_config=config['dashboard'],
    )


@app.route("/api/timeseries/<measurement>/<start:int>/<end:int>")
def serve_timeseries(measurement, start, end):
    """Route to serve timeseries.

    Parameters
    ----------
    measurement : `str`
        the measurement name
    start : `int`
        GPS start time
    end : `int`
        GPS end time

    """
    column, tags, _, tag_filters, aggregate, dt, _, datetime, backend = parse_query(bottle.request.query)

    consumer = config_to_consumer(app.config['backends'][backend])
    response = []

    ### query for timeseries
    if tag_filters:
        for tag in tag_filters:
            time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=[tag], aggregate=aggregate, dt=dt, datetime=datetime)
            response.append({'x':time, 'y':data, 'name': tag[1]})
    else:
        time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=tag_filters, aggregate=aggregate, dt=dt, datetime=datetime)
        response.append({'x':time, 'y':data})

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


@app.route("/api/segment/<measurement>/<start:int>/<end:int>")
def serve_segment(measurement, start, end):
    """Route to serve segment information.

    Parameters
    ----------
    measurement : `str`
        the measurement name
    start : `int`
        GPS start time
    end : `int`
        GPS end time

    """
    column, _, tag_key, tag_filters, aggregate, dt, far, datetime, backend = parse_query(bottle.request.query)

    consumer = config_to_consumer(app.config['backends'][backend])
    response = []

    ### query for segment plot
    if tag_filters:
        for tag in tag_filters:
            time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=[tag], aggregate=aggregate, dt=dt, datetime=datetime)
            # avg = sum(data) / len(data)
            response.append({'x':time, 'y':[.5], 'z':[data], 'name': tag[1]})
    else:
        time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=tag_filters, aggregate=aggregate, dt=dt, datetime=datetime)
        # avg = sum(data) / len(data)
        response.append({'x':time, 'y':[.5], 'z':[data]})

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


@app.route("/api/snapshot/<measurement>/<start:int>/<end:int>")
def serve_snapshot(measurement, start, end):
    """Route to serve snapshots, i.e. structured data for a single timestamp.

    Parameters
    ----------
    measurement : `str`
        the measurement name
    start : `int`
        GPS start time
    end : `int`
        GPS end time

    """
    consumer = config_to_consumer(app.config['backends'][backend])
    time, snapshot, dims = consumer.retrieve_snapshot(measurement)

    ### format request
    response = [{'x':snapshot[dims['x']], 'y':snapshot[dims['y']]}]

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response))


@app.route("/api/heatmap/<measurement>/<start:int>/<end:int>")
def serve_heatmap(measurement, start, end):
    """Route to serve heatmaps.

    Parameters
    ----------
    measurement : `str`
        the measurement name
    start : `int`
        GPS start time
    end : `int`
        GPS end time

    """
    column, _, tag_key, tag_filters, aggregate, dt, far, datetime, backend = parse_query(bottle.request.query)

    assert (len(column) == 1), 'column must contain only 1 element'
    column = column[0]

    ### query for timeseries
    consumer = config_to_consumer(app.config['backends'][backend])
    times, tags, datum = consumer.retrieve_binnedtimeseries_by_tag(measurement, start, end, column, tag_key, tags=tag_filters, aggregate=aggregate, dt=dt, datetime=datetime)

    ### format request
    response = [{'x':times, 'y':tags, 'z':datum}]

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


@app.route("/api/latest/<measurement>/<start:int>/<end:int>")
def serve_latest(measurement, start, end):
    """Route to serve the latest N points of a timeseries, keyed by tag.

    Parameters
    ----------
    measurement : `str`
        the measurement name
    start : `int`
        GPS start time
    end : `int`
        GPS end time

    """
    column, tags, _, tag_filters, aggregate, dt, _, datetime, backend = parse_query(bottle.request.query)

    tag = app.config['measurements'][measurement]['tag']
    default_value = app.config['measurements'][measurement]['default']
    transform = app.config['measurements'][measurement]['transform']
    y = []

    ### query for timeseries
    consumer = config_to_consumer(app.config['backends'][backend])
    current_gps = utils.gps_now()
    time, tag_ids, data = consumer.retrieve_latest_by_tag(measurement, column[0], tag_key=tag, aggregate=aggregate, dt=dt, datetime=datetime)
    for i in range(len(tag_ids)):
        y.append(transform_data(time[i], data[i], transform, default_value, current_gps))

    ### format request
    response = [{'x':tag_ids, 'y':y}]

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response))


@app.route("/api/ifar/<measurement>/<start:int>/<end:int>")
def serve_ifar(measurement, start, end):
    """Route to serve ifar plot-formatted data.

    Parameters
    ----------
    measurement : `str`
        the measurement name
    start : `int`
        GPS start time
    end : `int`
        GPS end time

    """
    columns, _, _, _, _, _, far, datetime, backend = parse_query(bottle.request.query)

    consumer = config_to_consumer(app.config['backends'][backend])
    response = []

    ### query for timeseries
    triggers = consumer.retrieve_triggers(measurement, start, end, columns, far=far, datetime=datetime)

    ### format request
    fars = sorted([1./ row['far'] for row in triggers], reverse=True)
    response = [{'x':fars, 'y':range(1, len(fars))}]

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


@app.route("/api/table/<measurement>/<start:int>/<end:int>")
def serve_table(measurement, start, end):
    """Route to serve dynamic tables.

    Parameters
    ----------
    measurement : `str`
        the measurement name
    start : `int`
        GPS start time
    end : `int`
        GPS end time

    """
    columns, _, _, _, _, _, far, datetime = parse_query(bottle.request.query)
    consumer = config_to_consumer(app.config['backends'][backend])
    response = []
    column_names = ['time']
    column_names.extend(columns)

    ### query for timeseries
    triggers = consumer.retrieve_triggers(measurement, start, end, columns, far=far, datetime=datetime)

    ### build field_dict
    field = []
    for col in column_names:
        field_dict = {'key': col, 'label': col, 'sortable': True}
        field.extend([field_dict])

    ### format request
    response = {'fields': field, 'items':triggers}

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


@app.route("/api/nagios/<check>")
def serve_nagios(check):
    """Route to serve JSON-formatted status information for nagios.

    Parameters
    ----------
    check : `str`
        the nagios check to perform, used as a lookup in the app configuration

    """
    nagios_config = app.config['nagios'][check]
    backend = nagios_config.get('backend', 'default')

    ### time settings
    duration = nagios_config['lookback']
    end = aggregator.now()
    start = end - duration
    dt = utils.duration_to_dt(duration)

    ### data settings
    measurement = nagios_config['measurement']
    column = nagios_config['column']
    tags = nagios_config['tags'] if 'tags' in nagios_config else []
    aggregate = nagios_config['aggregate']

    ### alert settings
    alert_type = nagios_config['alert_type']
    alert_tags = utils.extract_alert_tags(nagios_config['alert_settings'])

    ### alert tracking
    alert_values = []
    bad_status = 0
    now = utils.gps_now()

    ### retrieve data
    consumer = config_to_consumer(app.config['backends'][backend])
    for alert_tag in alert_tags:
        retrieve_tags = [alert_tag]
        retrieve_tags.extend(tags)
        time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=retrieve_tags, aggregate=aggregate, dt=dt)

        if alert_type == 'heartbeat':
            if time:
                alert_values.append(max(now - time[-1], 0))
            else:
                bad_status += 1

        elif alert_type == 'threshold':
            if data:
                max_data = max(data)
                if max_data >= nagios_config['alert_settings']['threshold']:
                    bad_status += 1

    ### format nagios response
    if bad_status:
        if alert_type == 'heartbeat':
            text_status = "{num_tags} {alert_tag} more than {lookback} seconds behind".format(
                alert_tag=nagios_config['alert_settings']['tag_type'],
                num_tags=bad_status,
                lookback=duration,
            )
        elif alert_type == 'threshold':
            text_status = "{num_tags} {alert_tag} above {column} threshold = {threshold} {units} from gps times: {start} - {end}".format(
                alert_tag=nagios_config['alert_settings']['tag_type'],
                threshold=nagios_config['alert_settings']['threshold'],
                units=nagios_config['alert_settings']['threshold_units'],
                num_tags=bad_status,
                column=measurement,
                start=start,
                end=end,
            )

    else:
        if alert_type == 'heartbeat':
            text_status = "OK: Max delay: {delay} seconds".format(delay=max(alert_values))
        elif alert_type == 'threshold':
            text_status = "OK: No {alert_tag}s above {column} threshold = {threshold} {units} from gps times: {start} - {end}".format(
                alert_tag=nagios_config['alert_settings']['tag_type'],
                threshold=nagios_config['alert_settings']['threshold'],
                units=nagios_config['alert_settings']['threshold_units'],
                column=measurement,
                start=start,
                end=end,
            )

    ### return response
    response = utils.status_to_nagios_response(text_status, bad_status=bad_status)
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response, sort_keys=True, indent=4, separators=(',', ': ')))


#-------------------------------------------------
### functions

def parse_query(query):
    columns = query.getall('column')
    #tags = query.getall('tag', [])
    tags = query.getall('tag')
    tag_key = query.get('tag_key', None)

    far = query.get('far', None)
    aggregate = query.get('aggregate', None)
    dt = query.get('dt', None)
    if dt:
        dt = int(dt)

    if 'datetime' in query:
        datetime = query['datetime'] == 'true'
    else:
        datetime = False

    tag_filters = [(key, val) for key, val in bottle.request.query.allitems() if key in tags]

    backend = query.get('backend', 'default')

    return columns, tags, tag_key, tag_filters, aggregate, dt, far, datetime, backend


def transform_data(time_value, data_value, transform, default_value, now):
    if transform == 'none':
        if data_value:
            return data_value
        else:
            return default_value
    elif transform == 'latency':
        if isinstance(time_value, list):
            return max(now - time_value[-1], 0)
        elif time_value:
            return max(now - time_value, 0)
        else:
            return default_value
    else:
        raise NotImplementedError('transform option not known/implemented, only "none" or "latency" are accepted right now')



def config_to_consumer(config):
    backend = config['backend']
    if backend == 'influxdb':
        return io.influx.Consumer(**config)
    elif backend == 'hdf5':
        return io.hdf5.Consumer(**config)
    else:
        raise NotImplementedError


def _add_parser_args(parser):
    parser.add_argument('-b', '--backend', default='wsgiref',
                        help="chooses server backend. options: [cgi|wsgiref]. default=wsgiref.")
    parser.add_argument('-c', '--config',
                        help="sets dashboard/plot options based on yaml configuration. if not set, uses SCALDRC_PATH.")
    parser.add_argument('-e', '--with-cgi-extension', default=False, action='store_true',
                        help="chooses whether scripts need to have a .cgi extension (if using cgi backend)")
    parser.add_argument('-n', '--application-name', default='scald',
                        help="chooses the web application name. default = scald.")


#-------------------------------------------------
### main

def main(args=None):
    """Serves data and dynamic html pages

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    ### parse args and set up configuration
    server_backend = args.backend
    app_name = args.application_name

    ### hacks to deal with running on apache and/or cgi
    use_cgi = (server_backend == 'cgi')
    if args.with_cgi_extension:
        app_name += '.cgi/'
    else:
        app_name += '/'
    script_name = app_name if use_cgi else ''

    ### load configuration
    if args.config:
        config_path = args.config
    else:
        config_path = os.getenv('SCALDRC_PATH')
    if not config_path:
        raise KeyError('no configuration file found, please set your SCALDRC_PATH correctly using "export SCALDRC_PATH=PATH/TO/CONFIG" or add --config param (-c /path/to/config)')

    with open(config_path, 'r') as f:
        app.config.update(yaml.safe_load(f))

    ### update configuration
    app.config.update({'script_name': script_name, 'use_cgi': use_cgi})

    ### start server
    bottle.run(app, server=server_backend, debug=True)
