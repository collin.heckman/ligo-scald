#!/usr/bin/env python

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__description__ = "a module for shared I/O utilities"

#-------------------------------------------------
### imports


import json
import os

from .. import aggregator

#-------------------------------------------------
### dir utilities


def gps_to_leaf_directory(gpstime, level = 0):
    """Get the leaf directory for a given gps time.

    """
    return "/".join(str(aggregator.gps_to_minimum_time_quanta(gpstime) // aggregator.MIN_TIME_QUANTA // (10**level)))


#-------------------------------------------------
### json utilities


def store_snapshot(webdir, measurement, data, dims, time, **attrs):
    """Stores a JSON-formatted snapshot to disk.

    Parameters
    ----------
    webdir : `str`
        the directory where snapshots are stored, should
        be web accessible (e.g. public_html)
    measurement : `str`
        the measurement name
    data : `dict`
        a mapping from a column to 1-dim data
    dims : `dict`
        a mapping from a dimension (one of x, y, z) to a column,
        either 2-dim (x, y) or 3-dim (x, y, z).
    time : `int`
        the time the snapshot was taken

    """
    ### set up JSON structure
    snapshot = {'time': time, 'measurement': measurement}
    snapshot.update(data)
    snapshot.update(dims)
    snapshot.update({'metadata': attrs})

    ### create directories
    leafdir = gps_to_leaf_directory(time)
    snapshot_dir = os.path.join(webdir, 'snapshots', leafdir)
    aggregator.makedir(snapshot_dir)

    ### save snapshot to disk
    filename = '{}_{}.json'.format(measurement, time)
    filepath = os.path.join(snapshot_dir, filename)
    with open(filepath, 'w') as f:
        f.write(json.dumps(snapshot))

    ### symlink latest snapshot
    sympath = os.path.join(webdir, 'snapshots', 'latest', '{}.json'.format(measurement))
    try:
        os.symlink(filepath, sympath)
    except OSError:
        os.remove(sympath)
        os.symlink(filepath, sympath)
