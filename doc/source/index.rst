.. ligo-scald documentation master file, created by
   sphinx-quickstart on Sun Jan  6 17:53:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ligo-scald's documentation!
======================================

.. toctree::
    :maxdepth: 2

    api/api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
