import os
from setuptools import setup

setup(
    name = 'ligo-scald',
    description = 'SCalable Analytics for Ligo Data',
    version = '0.4.1',
    author = 'Patrick Godwin',
    author_email = 'patrick.godwin@ligo.org',
    url = 'https://git.ligo.org/gstlal-visualisation/ligo-scald.git',
    license = 'GPLv2+',

    packages = ['ligo', 'ligo.scald', 'ligo.scald.io', 'static', 'templates'],
    namespace_packages = ['ligo'],

    package_data = {
        'static': ['*'],
        'templates': ['*'],
    },

    entry_points={
        'console_scripts': [
            'scald = ligo.scald.__main__:main',
        ],
    },
)
